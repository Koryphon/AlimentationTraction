#ifndef __MesureFCEM_h__
#define __MesureFCEM_h__

#include "Arduino.h"

class MesureFCEM {
  private:
    byte mPin;
    unsigned int mEchantillons[6];
  
  public:
    MesureFCEM(byte pin) : mPin(pin) { razEchantillons(); }
    void init();
    int echantillonne();  
    void razEchantillons();
};

extern MesureFCEM mesureFcem;

#endif


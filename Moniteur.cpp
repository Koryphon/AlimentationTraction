#include <CommandInterpreter.h>
#include "Moniteur.h"
#include "PinDefinitions.h"
#include "RelaiSens.h"
#include "CommandePWM.h"
#include "IHM.h"
#include "Enregistreur.h"

CommandInterpreter CLI(Serial);

/*-----------------------------------------------------------------------------
 * Commande le relai de sens de circulation
 */
void fonctionCommandeRelai(CommandInterpreter &cmdInt, byte argCount)
{
  char *sens;
  HardwareSerial &serial = cmdInt.Serial();
  byte etat;
  
  switch (argCount) {
    case 0: /* lecture du sens */
      etat = relai.etat();
      if (etat == LOW) serial.println(F("avant"));
      else serial.println(F("arriere"));
      break;
    case 1: /* ecriture du sens */
      cmdInt.readString(sens);
      if (strcmp(sens, "av") == 0) {
        if (relai.etat() == HIGH && PWMMoteur.pwm() != 0) {
          serial.println(F("La PWM doit etre a 0 avant de changer de sens"));
        }
        else
          relai.avant();
      }
      else if (strcmp(sens, "ar") == 0) {
        if (relai.etat() == LOW && PWMMoteur.pwm() != 0) {
          serial.println(F("La PWM doit etre a 0 avant de changer de sens"));
        }
        else
          relai.arriere();
      }
      else serial.println(F("Argument : 'av' ou 'ar'"));
      break;
    default:
      serial.println(F("Usage: 'sens' affiche le sens, 'sens av' va en avant 'sens ar' va en arriere"));
      break;
  }
}

Command cmdRelaiSens("sens", fonctionCommandeRelai, "Commande le relai de sens");

/*-----------------------------------------------------------------------------
 * Commande pour démarrer l'enregistrement de la FCEM et de l'erreur
 */
//void fonctionEnregistrement(CommandInterpreter &cmdInt, byte argCount)
//{
//  HardwareSerial &serial = cmdInt.Serial();
//  char *onOff;
//  
//  if (argCount == 0) enregistre = true;
//  else serial.println(F("Usage: de"));
//}
//
//Command cmdEnregistre("de", fonctionEnregistrement, "Demarre l'enregistrement de la FCEM et de l'erreur");

/*-----------------------------------------------------------------------------
 * Commande pour afficher l'enregistreur d'erreur
 */
//void fonctionAfficheErreur(CommandInterpreter &cmdInt, byte argCount)
//{
//  HardwareSerial &serial = cmdInt.Serial();
//  char *onOff;
//  
//  if (argCount == 0) enregistreurErreur.affiche(serial, ' ');
//  else serial.println(F("Usage: erreurs"));
//}
//
//Command commandeAfficheErreurs("erreurs", fonctionAfficheErreur, "Affiche l'enregistrement des erreurs");

/*-----------------------------------------------------------------------------
 * Commande pour lire ou définir le gain proportionnel
 */
void fonctionGainProportionnel(CommandInterpreter &cmdInt, byte argCount)
{
  HardwareSerial &serial = cmdInt.Serial();
  unsigned int gainProportionnel;
  
  switch (argCount) {
    case 0: 
      serial.println(commandeMoteur.gainProportionnel());
      break;
    case 1:
      if (cmdInt.readUnsignedInt(gainProportionnel)) {
        commandeMoteur.defGainProportionnel(gainProportionnel);
      }
      else {
        serial.println(F("Le gain doit etre un entier positif"));
      }
      break;
    default:
      serial.println(F("Usage: 'gp' affiche le gain proportionnel, 'gp <gain>' definit le gain proportionnel. <gain> est un entier positif"));
      break;
  }
}

Command cmdGainProportionnel("gp", fonctionGainProportionnel, "affiche ou definit le gain proportionnel");

/*-----------------------------------------------------------------------------
 * Commande pour lire ou définir le gain intégral
 */
void fonctionGainIntegral(CommandInterpreter &cmdInt, byte argCount)
{
  HardwareSerial &serial = cmdInt.Serial();
  unsigned int gainIntegral;
  
  switch (argCount) {
    case 0: 
      serial.println(commandeMoteur.gainIntegral());
      break;
    case 1:
      if (cmdInt.readUnsignedInt(gainIntegral)) {
        commandeMoteur.defGainIntegral(gainIntegral);
      }
      else {
        serial.println(F("Le gain doit etre un entier positif"));
      }
      break;
    default:
      serial.println(F("Usage: 'gi' affiche le gain integral, 'gi <gain>' définit le gain integral. <gain> en un entier positif"));
      break;
  }
}

Command cmdGainIntegral("gi", fonctionGainIntegral, "affiche ou definit le gain integral");

/*-----------------------------------------------------------------------------
 * Commande pour définir la consigne
 */
void fonctionConsigne(CommandInterpreter &cmdInt, byte argCount)
{
  HardwareSerial &serial = cmdInt.Serial();
  unsigned int consigne;
  
  switch (argCount) {
    case 0: 
      serial.println(commandeMoteur.consigne());
      break;
    case 1:
      if (cmdInt.readUnsignedInt(consigne)) {
        commandeMoteur.defConsigne(consigne);
        // enregistreurErreur.demarre();
      }
      else {
        serial.println(F("la consigne est un entier positif"));
      }
      break;
    default:
      serial.println(F("Usage: 'cs' affiche la consigne, 'cs <valeur>' definit la consigne. <valeur> est un entier positif"));
      break;
  }
}

Command cmdConsigne("cs", fonctionConsigne, "affiche ou definit la consigne");

/*-----------------------------------------------------------------------------
 * Command to set the control on or off
 */
void fonctionCommande(CommandInterpreter &cmdInt, byte argCount)
{
  HardwareSerial &serial = cmdInt.Serial();
  char *theArg;
  
  switch (argCount) {
    case 0:
      if (commandeMoteur.estAsservi()) serial.println(F("on"));
      else serial.println(F("off"));
      break;
    case 1:
      cmdInt.readString(theArg);
      if (strcmp(theArg, "on") == 0) commandeMoteur.defAsservi(true);
      else if (strcmp(theArg, "off") == 0) commandeMoteur.defAsservi(false);
      else serial.println(F("l'argument doit etre 'on' or 'off'"));
      break;
    default:
      serial.println(F("Usage: 'com' affiche l'etat de la commande, 'com <valeur>' definit l'etat de la commande. <valeur> est 'on' ou 'off'"));
      break;
  }
}

Command cmdCommande("com", fonctionCommande, "affiche ou defini l'etat de la commande");

/*-----------------------------------------------------------------------------
 * Command to set the pwm divider
 */
void fonctionDiviseurPWM(CommandInterpreter &cmdInt, byte argCount)
{
  HardwareSerial &serial = cmdInt.Serial();
  byte diviseur;
  
  switch (argCount) {
    case 0: 
      serial.println(commandeMoteur.diviseurPWM());
      break;
    case 1:
      if (cmdInt.readByte(diviseur) && diviseur < 16) {
        commandeMoteur.defDiviseurPWM(diviseur);
      }
      else {
        serial.println(F("le diviseur doit etre un entier < 16"));
      }
      break;
    default:
      serial.println(F("Usage: 'divp' affiche le diviseur, 'divp <valeur>' définit le diviseur. <valeur> est un entier < 16"));
      break;
  }
}

Command cmdDiviseurPWM("divp", fonctionDiviseurPWM, "affiche ou definit le diviseur PWM");

/*-----------------------------------------------------------------------------
 * Command to set the maximum error
 */
void fonctionErreurMax(CommandInterpreter &cmdInt, byte argCount)
{
  HardwareSerial &serial = cmdInt.Serial();
  unsigned long erreur;
  
  switch (argCount) {
    case 0: 
      serial.println(commandeMoteur.erreurMax());
      break;
    case 1:
      if (cmdInt.readUnsignedLong(erreur)) {
        commandeMoteur.defErreurMax(long(erreur));
      }
      else {
        serial.println(F("erreur doit etre un entier positif"));
      }
      break;
    default:
      serial.println(F("Usage: 'emax' affiche l'erreur max, 'emax <valeur>' definit l'ereur max. <valeur> est un entier positif"));
      break;
  }
}

Command cmdErreurMax("emax", fonctionErreurMax, "affiche ou definit l'erreur maximum");

/*-----------------------------------------------------------------------------
 * Command to set the esum
 */
void fonctionSommeErreur(CommandInterpreter &cmdInt, byte argCount)
{
  HardwareSerial &serial = cmdInt.Serial();
  unsigned long erreur;
  
  switch (argCount) {
    case 0: 
      serial.println(commandeMoteur.sommeErreur());
      break;
    case 1:
      if (cmdInt.readUnsignedLong(erreur)) {
        commandeMoteur.defSommeErreur(erreur);
      }
      else {
        serial.println(F("la somme erreur doit etre un entieur positif"));
      }
      break;
    default:
      serial.println(F("Usage: 'somme' affiche la somme des erreurs, 'somme <valeur>' definit la somme des erreurs. <valeur> est un entier positif"));
      break;
  }
}

Command cmdSommeErreur("somme", fonctionSommeErreur, "affiche ou definit la somme des erreurs");

/*-----------------------------------------------------------------------------
 * Command to get the error
 */
void fonctionErreur(CommandInterpreter &cmdInt, byte argCount)
{
  HardwareSerial &serial = cmdInt.Serial();
  long erreur;
  
  if (argCount == 0) serial.println(commandeMoteur.erreur());
  else serial.println(F("Usage: 'err' affiche l'erreur"));
}

Command cmdErreur("err", fonctionErreur, "affiche l'erreur");

/*-----------------------------------------------------------------------------
 * Command for direct access to charlieplexing leds
 */
void fonctionLED(CommandInterpreter &cmdInt, byte argCount)
{
  HardwareSerial &serial = cmdInt.Serial();
  char *arg;
  bool afficheUsage = false;
  
  if (argCount == 0) leds.afficheEtat(serial);
  else if (argCount == 2) {
    byte etat;
    /* get the on name argument */
    cmdInt.readString(arg);
    int8_t uneLed = leds.nomExiste(arg);
    
    if (uneLed == -1) afficheUsage = true;
    
    if (! afficheUsage) {
      /* get the on of off argument */
      cmdInt.readString(arg);
      if (strcmp(arg,"on") == 0) etat = HIGH;
      else if (strcmp(arg,"off") == 0) etat = LOW;
      else afficheUsage = true;
    }
    
    if (! afficheUsage) leds.defEtat(uneLed, etat);
  }
  else afficheUsage = true;
  
  if (afficheUsage) serial.println(F("Usage: 'led <nom> <on/off>' definit la led <nom> on ou off. <nom> est : rx, tx, dar, dpv, cc or blk"));
}

Command cmdLED("led", fonctionLED, "definit l'etat d'une led");

void initMoniteur()
{
  CLI.setPrompt(F("Alim Traction>"));
  CLI.addCommand(cmdRelaiSens);
//  CLI.addCommand(cmdEnregistre);
//  CLI.addCommand(commandeAfficheErreurs);
  CLI.addCommand(cmdGainProportionnel);
  CLI.addCommand(cmdGainIntegral);
  CLI.addCommand(cmdConsigne);
  CLI.addCommand(cmdCommande);
  CLI.addCommand(cmdDiviseurPWM);
  CLI.addCommand(cmdErreurMax);
  CLI.addCommand(cmdSommeErreur);
  CLI.addCommand(cmdErreur);
  CLI.addCommand(cmdLED);
}

void executeMoniteur()
{
  CommandInterpreter::update();
}

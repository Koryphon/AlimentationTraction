#include "MesureFCEM.h"

static const unsigned char ADC_PS_16 = (1 << ADPS2);
static const unsigned char ADC_PS_32 = (1 << ADPS2) | (1 << ADPS0);
static const unsigned char ADC_PS_64 = (1 << ADPS2) | (1 << ADPS1);
static const unsigned char ADC_PS_128 = (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
static const unsigned char ADC_PS_CLEAR = ADC_PS_128;

bool enregistre;

/*-----------------------------------------------------------------------------
 * Initialise le convertisseur analogique numérique
 */
void MesureFCEM::init()
{
  /* Change la fréquence de 125kHz, valeur par défaut sur l'Arduino, à 1MHz,
   * valeur maximum préconisée.
   * Voir paragraphe 2.8 de http://www.atmel.com/Images/doc2559.pdf
   * Donc le prescaler est mis à 16 : ADC_PS_16 */
  /* Clear previous settings */
  ADCSRA &= ~ADC_PS_CLEAR;
  ADCSRA |= ADC_PS_16;
}

int MesureFCEM::echantillonne()
{
  for (byte i = 0; i < 6; i++) {
    /* Chaque échantillon est entre 0 et 1023 */
    unsigned int echantillon = analogRead(mPin);
    /* Stocke les échantillons dans un tableau trié */
    for (byte j = 0; j < 6; j++) {
      if (echantillon < mEchantillons[j]) {
        unsigned int tmp = mEchantillons[j];
        mEchantillons[j] = echantillon;
        echantillon = tmp;
      }
    }
  }
  /* Effectue la sommes des 4 éléments centraux du tableau */
  int fcem = 0;
  for (byte i = 1; i < 5; i++) {
    fcem += mEchantillons[i];
  }
  /* La somme est entre 0 et 4092 */
  /* On divise par 4 */
  return fcem >> 2;
}

void MesureFCEM::razEchantillons()
{
  for (byte i = 0; i < 6; i++) mEchantillons[i] = 0xFFFF;
}

MesureFCEM mesureFcem(A6);


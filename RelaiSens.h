#ifndef __RelaiSens_h__
#define __RelaiSens_h__

#include "Arduino.h"
#include "QuickPort.h"

class RelaiSens {
  private:
    QuickPort portRelai;
    
  public:
    RelaiSens(byte pin);
    void init();
    void avant()
    {
      portRelai.writeLOW();
    }
    
    void arriere()
    {
      portRelai.writeHIGH();
    }
    
    byte etat()
    {
      return portRelai.read();
    }
};

extern RelaiSens relai;

#endif

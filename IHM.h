#ifndef __IHM_h__
#define __IHM_h__

#include "Arduino.h"
#include "QuickPort.h"

static const byte CAN_RX_LED = 0;
static const byte CAN_TX_LED = 1;
static const byte BLINK_LED  = 2;
static const byte DETECTION_PV_LED = 3;
static const byte DETECTION_AR_LED = 4;
static const byte COURT_CIRCUIT_LED = 5;

class CharlieLED
{
  private:
    bool mAllume[6];
    byte mIndex;
    unsigned long mLastDate;
    
  public:
    CharlieLED();
    void canRX() { mAllume[CAN_RX_LED] = ! mAllume[CAN_RX_LED]; }
    void canTX() { mAllume[CAN_TX_LED] = ! mAllume[CAN_TX_LED]; }
    void blink() { mAllume[BLINK_LED]  = ! mAllume[BLINK_LED];  }
    void detectionPleineVoie(bool detection) { mAllume[DETECTION_PV_LED] = detection; }
    void detectionZoneArret(bool detection)  { mAllume[DETECTION_AR_LED] = detection; }
    void courtCircuit(bool cc) { mAllume[COURT_CIRCUIT_LED] = cc; }
    void defEtat(byte led, byte state) { mAllume[led] = (state == HIGH); }
    void maj();
    void afficheEtat(HardwareSerial& serial);
    int8_t nomExiste(char *name);
};

extern CharlieLED leds;

#endif


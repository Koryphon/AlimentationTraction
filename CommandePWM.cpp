#include "CommandePWM.h"
#include "MesureFCEM.h"
#include "PinDefinitions.h"
#include "Enregistreur.h"

PWM PWMMoteur;

void CommandePWM::init()
{
  digitalWrite(pinActiverShuntDetection, LOW);
  pinMode(pinActiverShuntDetection, OUTPUT);
  PWMMoteur.init();
}

void CommandePWM::prepareCommande()
{
  PWMMoteur.suspendre();
  digitalWrite(pinActiverShuntDetection, HIGH);
}

void CommandePWM::commande()
{
  /* Mesure la FCEM */
  int fcem = mesureFcem.echantillonne();
  long pwm;
  
  if (mAsservi && mConsigne != 0) {
    /* Calcule l'erreur */
    int erreur = mConsigne - fcem;
    mErreur = erreur;
//    enregistreurErreur.ajoute(error);
    /* accumule l'erreur */
    mSommeErreur += erreur * mGainIntegral;
    if (mSommeErreur > mErreurMax) mSommeErreur = mErreurMax;
    else if (mSommeErreur < -mErreurMax) mSommeErreur = -mErreurMax;
    /* calcule la PWM */
    pwm = (erreur * mGainProportionnel + mSommeErreur) >> mDiviseurPWM ;
    if (pwm < 0) pwm = 0;
    else if (pwm > 255) pwm = 255;
  }
  else {
    mSommeErreur = 0;
    pwm = mConsigne >> 2;
  }
  digitalWrite(pinActiverShuntDetection, LOW);
  PWMMoteur.defPWM(byte(pwm));
}

CommandePWM commandeMoteur;


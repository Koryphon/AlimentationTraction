#include "Enregistreur.h"

void Enregistreur::affiche(HardwareSerial &serial, char separateur)
{
  for (byte i = 0; i < 255; i++) {
    serial.print(mEchantillons[i]);
    serial.write(separateur);
  }
  serial.print(mEchantillons[255]);
}

// Enregistreur enregistreurErreur;

